# CloudWatch Subscribers

The purpose of the functions in this repository is to be able to subscribe new and existing CloudWatch log group events to a particular resource e.g. a Lambda function.

For subscribing new log groups, the serverless function included may be deployed and used. However, it does have a dependency of having an existing CloudTrail trail set up for your AWS account, with an arbitrary name and S3 bucket.

For subscribing existing logs groups, no preexisting resources are required, and the test in `test_subscribe_existing` should be used.

In both cases, some environment variables need to be set. These are in addition to any required to hook up your AWS account to Serverless:
- DESTINATION_ARN: The ARN of the resource you wish to receive logs from
- FILTER_NAME: The name of a subscription filter you may wish to apply to the logs
- FILTER_PATTERN: The pattern for the above filter. See [here](https://docs.aws.amazon.com/AmazonCloudWatch/latest/logs/FilterAndPatternSyntax.html) for examples. Our current choice is to use the `[w1!=START&&w1!=END&&w1!=REPORT,]` pattern to remove AWS Lambda context logs that are not part of the source code.

## Subscribing new log groups

After cloning this repository, environment variables are required. These should be provided in a `secrets.yml` file at the top level of the repository, and should take the following form:

```yaml
DESTINATION_ARN: <your destination ARN>
FILTER_NAME: <your filter name>
FILTER_PATTERN: <your filter pattern>
```

Deploy the lambda using
```bash
yarn
yarn sls deploy
```

No further steps should be required, and the deployed lambda should be ready to subscribe new log groups.

## Subscribing existing log groups

Unlike subscribing new log groups, subscribing existing ones should be a one-off task. For this reason it has been left as a manual step. Manually subscribing and unsubscribing existing log groups to a particular resource can be achieved by running either test in `functions/tests/test_subscribe_existing.py`.

Here, the relevant environment variables may be set directly in each function in the file, instead of using a `secrets.yml` file.