import os

import boto3


def get_client():
    return boto3.client('logs', region_name='us-east-1')


def subscribe_existing_log_groups():
    client = get_client()

    destination_arn = os.getenv("DESTINATION_ARN")
    filter_name = os.getenv("FILTER_NAME")
    filter_pattern = os.getenv("FILTER_PATTERN")

    response = client.describe_log_groups(
        # logGroupNamePrefix='string',
    )
    for log_group in response["logGroups"]:
        log_group_name = log_group["logGroupName"]
        subscription_response = client.describe_subscription_filters(
            logGroupName=log_group_name,
        )

        if len(subscription_response["subscriptionFilters"]) == 0:
            try:
                client.put_subscription_filter(
                    logGroupName=log_group_name,
                    filterName=filter_name,
                    filterPattern=filter_pattern,
                    destinationArn=destination_arn,
                )
                print(f"Subscribed {log_group_name} with filter {filter_name}")
            except:
                # An exception is thrown when the loggroup from the logzio function tries to subscribe
                # to that function, which makes sense, as it would be an infinite loop and not good!
                pass


def unsubscribe_all_log_groups():
    client = get_client()

    filter_name = os.getenv("FILTER_NAME")

    response = client.describe_log_groups(
        # logGroupNamePrefix='string',
    )
    for log_group in response["logGroups"]:
        log_group_name = log_group["logGroupName"]
        subscription_response = client.describe_subscription_filters(
            logGroupName=log_group_name,
        )

        if len(subscription_response["subscriptionFilters"]) > 0:
            print(log_group_name)
            print(subscription_response)
            subscription_filter_names = [
                x["filterName"]
                for x
                in subscription_response["subscriptionFilters"]
            ]
            if filter_name in subscription_filter_names:
                response = client.delete_subscription_filter(
                    logGroupName=log_group_name,
                    filterName=filter_name
                )
                print(response)
