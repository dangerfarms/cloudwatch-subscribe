import os

import boto3


def get_client():
    return boto3.client('logs', region_name='us-east-1')


def subscribe_new_log_group(event, context):
    destination_arn = os.getenv("DESTINATION_ARN")
    filter_name = os.getenv("FILTER_NAME")
    filter_pattern = os.getenv("FILTER_PATTERN")

    client = get_client()

    print(f"event: {event}")

    log_group_name = event["detail"]["requestParameters"]["logGroupName"]
    print(f"Log group name: {log_group_name}")

    try:
        client.put_subscription_filter(
            logGroupName=log_group_name,
            filterName=filter_name,
            filterPattern=filter_pattern,
            destinationArn=destination_arn,
        )
        print(f"Subscribed {log_group_name} with filter {filter_name}")
    except:
        # An exception is thrown when the loggroup from the logzio function tries to subscribe
        # to that function, which makes sense, as it would be an infinite loop and not good!
        pass
