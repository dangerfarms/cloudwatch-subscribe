import os
import unittest

from functions.subscribe_existing import subscribe_existing_log_groups, unsubscribe_all_log_groups


class SubscribeExistingLogGroupTestCase(unittest.TestCase):
    @staticmethod
    def test_subscribe_existing_log_groups():
        os.environ["AWS_ACCESS_KEY_ID"] = "CHANGE ME"
        os.environ["AWS_SECRET_ACCESS_KEY"] = "CHANGE ME"
        os.environ["DESTINATION_ARN"] = "CHANGE ME"
        os.environ["FILTER_NAME"] = "filter-start-stop-report"
        os.environ["FILTER_PATTERN"] = "[w1!=START&&w1!=END&&w1!=REPORT,]"

        subscribe_existing_log_groups()

    @staticmethod
    def test_unsubscribe_existing_log_groups():
        os.environ["AWS_ACCESS_KEY_ID"] = "CHANGE ME"
        os.environ["AWS_SECRET_ACCESS_KEY"] = "CHANGE ME"
        os.environ["FILTER_NAME"] = "filter-start-stop-report"

        unsubscribe_all_log_groups()
