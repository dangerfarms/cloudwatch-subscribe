import json
import os
import unittest

from functions.subscribe_new import subscribe_new_log_group


class SubscribeNewLogGroupTestCase(unittest.TestCase):
    @staticmethod
    def test_subscribe_new_lambda():
        os.environ["AWS_ACCESS_KEY_ID"] = "CHANGE ME"
        os.environ["AWS_SECRET_ACCESS_KEY"] = "CHANGE ME"

        event = json.loads(
            """
            ADD TEST CLOUDWATCH EVENT HERE
            """
        )

        subscribe_new_log_group(event, None)
